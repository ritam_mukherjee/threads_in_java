package thread_memory;


/**
 * All thread use global object in same heap space
 */
public class ThreadMemoryProgram1 {

    public static void main(String[] args) throws InterruptedException {
        Object globalObject=new Object();  /*Global Variable*/

        Runnable runnable = () -> {

            Object localObject=new Object();  /*Local  Variable*/
            System.out.println(ThreadColor.ANSI_RESET+"The \'"
                    +Thread.currentThread().getName()+" thread\' object references are :");
            System.out.println(
                    ThreadColor.getThreadColor(Thread.currentThread().getName())+
                            "\tThe local object hashcode:"+localObject.hashCode());
            System.out.println(
                    ThreadColor.getThreadColor(Thread.currentThread().getName())+
                            "\tThe global object hashcode:"+globalObject.hashCode());

        };

        Thread redThread= new Thread(runnable,"red");
        Thread cyanThread= new Thread(runnable,"cyan");
        Thread greenThread= new Thread(runnable,"green");
        Thread yellowThread= new Thread(runnable,"yellow");

        redThread.start();
        redThread.join();

        cyanThread.start();
        cyanThread.join();

        greenThread.start();
        greenThread.join();

        yellowThread.start();
        yellowThread.join();

    }

}
