package data_race.race_problem_resolve;

import thread_memory.ThreadColor;

class CounterIncrementerThread extends Thread {

    int count = 0;

    @Override
    public void run() {

        for (int i = 0; i < 10_000_000; i++) {
            count++;
        }
        System.out.println(
                ThreadColor.getThreadColor(Thread.currentThread().getName()) +
                        "The processed count variable and the no is:" + count);

    }
}
