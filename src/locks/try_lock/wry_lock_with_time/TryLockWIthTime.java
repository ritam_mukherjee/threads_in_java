package locks.try_lock.wry_lock_with_time;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by ritmukherjee on 1/9/2017.
 */

public class TryLockWIthTime {
    static ReentrantLock rl=new ReentrantLock();
    static int attempt=1;
    static Thread createThread(String name){
        Thread thread=new Thread(
            /* First parameter:RunnableObject     */
                ()->{
                    do{
                        try {
                            if(rl.tryLock(5000, TimeUnit.MILLISECONDS)){
                                System.out.println(
                                        ThreadColor.getThreadColor.apply(Thread.currentThread().getName())
                                        + Thread.currentThread().getName() +
                                        " thread.. got lock ");
                                Thread.sleep(30000);
                                System.out.println(
                                        ThreadColor.getThreadColor.apply(Thread.currentThread().getName())
                                                + Thread.currentThread().getName() +
                                                "thread.. going to release lock ");
                                rl.unlock();
                                attempt=0;
                                break;
                            }else{
                                System.out.println(
                                        ThreadColor.getThreadColor.apply(Thread.currentThread().getName())+"\t\t"
                                        + "Attempt:"+attempt+",.. "+ Thread.currentThread().getName()+
                                        " thread unable to get lock" );
                                attempt++;
                            }
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }while(true);
        }
            /*Second parameter:name*/
         ,name);
        return thread;
    }
    public static void main(String[] args) {
        createThread("red").start();
        createThread("green").start();
    }
}
