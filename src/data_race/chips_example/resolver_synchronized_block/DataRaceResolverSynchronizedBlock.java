package data_race.chips_example.resolver_synchronized_block;

import race_condition.ThreadColor;

/**
 * 1. In this case need to get class level lock and that block need to be  'synchronized'
 * 2. Even we make the counter object reference type we will not get object lock reason that is mutable instance
 *   and value once create can't change, hence for every incremented value new instance will be created for each value
 *   hence, synchronization can't achieve.
 */
class PartyShopping extends Thread {

    public static int bagsOfChips = 0; // start with one on the list

    public PartyShopping(String name) {
        this.setName(name);
    }


    public void run() {
        for (int i = 0; i < 10_000_000; i++) {
         /*   synchronized (this) if we use this we will not get desired output as object level lock*/
           synchronized (this.getClass()){
               bagsOfChips++;
           }
        }
    }
}

public class DataRaceResolverSynchronizedBlock {
    public static void main(String[] args) throws InterruptedException {
        Thread greenThread=new PartyShopping("green");
        Thread redThread=new PartyShopping("red");
        redThread.start();
        greenThread.start();

        redThread.join();
        greenThread.join();

        System.out.println(ThreadColor.ANSI_BLUE+"No of chips will be purchases "+PartyShopping.bagsOfChips);
    }
}
