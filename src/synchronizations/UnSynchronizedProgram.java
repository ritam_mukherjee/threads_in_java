package synchronizations;

import java.util.stream.IntStream;

public class UnSynchronizedProgram {

    public  void execute(String color) {
        IntStream.range(0,5).forEach(value -> {
            try {
                Thread.sleep(2000);
                System.out.println(ThreadColor.getThreadColor(color)
                        +Thread.currentThread().getName()+" thread is Now executing, count: "+value);
            } catch (InterruptedException ie) {
                System.out.println(ie);
            }
        });
    }

    public static void main(String[] args) {
        Thread redThread=new Thread(() -> new UnSynchronizedProgram().execute("red"),"Red");
        redThread.start();

        Thread greenThread=new Thread(() -> new UnSynchronizedProgram().execute("green"),"Green");
        greenThread.start();

        Thread cyanThread=new Thread(() -> new UnSynchronizedProgram().execute("cyan"),"Cyan");
        cyanThread.start();

        Thread yellowThread=new Thread(() -> new UnSynchronizedProgram().execute("yellow"),"Yellow");
        yellowThread.start();

        Thread purpleThread=new Thread(() -> new UnSynchronizedProgram().execute("purple"),"Purple");
        purpleThread.start();
    }

}
