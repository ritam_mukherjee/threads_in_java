package locks.try_lock.try_lock_interruptable;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Here one thread get lock but other threads are not waiting for that.
 * but the lock is interruptable
 * if that happen other thread get chance to execute
 */
public class TryLockInterruptable {
    public static void main(String[] args) throws InterruptedException {

        Lock lock = new ReentrantLock();

        Runnable runnable = () -> {
            System.out.println(ThreadColor.getThreadColor.apply(Thread.currentThread().getName())
                    + Thread.currentThread().getName() +
                    " thread.... is trying to get lock");

            if (lock.tryLock()) {
                System.out.println(ThreadColor.getThreadColor.apply(Thread.currentThread().getName())
                        + Thread.currentThread().getName() +
                        " thread.. got lock performing safe operation");
                try {
                    lock.lockInterruptibly();
                    for (int i = 1; i < 5; i++) {
                        try {
                            System.out.println(ThreadColor.getThreadColor.apply(Thread.currentThread().getName()) + "\t\t"
                                    + Thread.currentThread().getName()
                                    + " now executing :" + i);
                            Thread.sleep(3000);
                        } catch (Exception e) {
                            System.out.println(ThreadColor.ANSI_WHITE + " Interruption Happened...");
                        }
                    }
                } catch (InterruptedException e) {
                    System.out.println(ThreadColor.ANSI_WHITE + " Interruption Happened...");
                }
                /* releasing lock*/
                System.out.println(ThreadColor.getThreadColor.apply(Thread.currentThread().getName())
                        + Thread.currentThread().getName() +
                        " thread.. is going to release lock");
                lock.unlock();
            } else {
                System.out.println(ThreadColor.getThreadColor.apply(Thread.currentThread().getName())
                        + Thread.currentThread().getName()
                        + "thread..is  unable to get lock");
            }
        };
        Thread green_thread = new Thread(runnable, "green");
        green_thread.setPriority(2);
        Thread red_thread = new Thread(runnable, "red");
        red_thread.setPriority(4);
        Thread cyan_thread = new Thread(runnable, "cyan");
        cyan_thread.setPriority(6);
        Thread purple_thread = new Thread(runnable, "purple");
        purple_thread.setPriority(8);
        Thread blue_thread = new Thread(runnable, "blue");
        blue_thread.setPriority(10);

        green_thread.start();
        red_thread.start();
        cyan_thread.start();
        blue_thread.start();
        purple_thread.start();
        red_thread.interrupt();

    //    Thread.sleep(2000);
        purple_thread.interrupt();
        blue_thread.interrupt();
        green_thread.interrupt();
        cyan_thread.interrupt();
        /*   Thread.sleep(200);*/

        /*Thread.sleep(200);*/
        /**/


    }
}
