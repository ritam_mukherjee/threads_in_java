package data_race.chips_example.resolver_synchronized_method;

import race_condition.ThreadColor;

/**
 * 1. In this case need a static method and mark that 'synchronized'
 * 2. We need to mark the method as static  to aquire 'class level lock'
 *  other wise it will get object level lock
 * 3. Object level lock will not give us desired result
 */
class PartyShopping extends Thread {

    public static int bagsOfChips = 0; // start with one on the list

    public PartyShopping(String name) {
        this.setName(name);
    }

    /*here we are making method synchronized
    This is acquiring class level lock as method is static
    We can't make it object level because then lock will be instance level which is not shared*/
    private static synchronized void addChips(){
        bagsOfChips++;
    }

    public void run() {
        for (int i = 0; i < 10_000_000; i++) {
            addChips();
        }
    }
}

public class DataRaceResolverSynchronizedMethod {
    public static void main(String[] args) throws InterruptedException {
        Thread greenThread=new PartyShopping("green");
        Thread redThread=new PartyShopping("red");
        redThread.start();
        greenThread.start();

        redThread.join();
        greenThread.join();

        System.out.println(ThreadColor.ANSI_BLUE+"No of chips will be purchases "+PartyShopping.bagsOfChips);
    }
}
