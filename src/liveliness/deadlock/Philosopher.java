package liveliness.deadlock;

import java.util.concurrent.locks.Lock;

class Philosopher extends Thread {

    private Lock leftSpoon, rightSpoon;
    /*if the count of friedChicken is less no issue but if the count is more then deadlock happens*/
    private static int friedChicken = 500_00_000;

    public Philosopher(String name, Lock leftSpoon, Lock rightSpoon) {
        this.setName(name);
        this.leftSpoon = leftSpoon;
        this.rightSpoon = rightSpoon;
    }

    public void run() {
        while (friedChicken > 0) { // eat sushi until it's all gone

            // pick up spoons
            leftSpoon.lock();
            rightSpoon.lock();

            // take a piece of fried chicken
            if (friedChicken > 0) {
                friedChicken--;
                System.out.println(
                        ThreadColor.getThreadColor.apply(this.getName())
                                + "The " + this.getName() + " philosopher took a piece! fried chicken," +
                                "remaining fried chicken count is : " + friedChicken);
            }

            // put down spoons
            rightSpoon.unlock();
            leftSpoon.unlock();
        }
    }
}
