package synchronizations;

public class SynchronizedBlockProgram {
    public void execute(String color) {
        synchronized (this) {
            for (int i = 0; i < 5; i++) {
                try {
                    Thread.sleep(1000);
                    System.out.println(ThreadColor.getThreadColor(color)
                            + Thread.currentThread().getName() + " thread is Now executing, count: "+i);
                } catch (InterruptedException ie) {
                    System.out.println(ie);
                }
            }
        }
    }

    public static void main(String[] args) {
        SynchronizedBlockProgram program = new SynchronizedBlockProgram();
        Thread redThread = new Thread(() -> program.execute("red"), "Red");
        redThread.start();

        Thread greenThread = new Thread(() -> program.execute("green"), "Green");
        greenThread.start();

        Thread cyanThread = new Thread(() -> program.execute("cyan"), "Cyan");
        cyanThread.start();

        Thread yellowThread = new Thread(() -> program.execute("yellow"), "Yellow");
        yellowThread.start();

        Thread purpleThread = new Thread(() -> program.execute("purple"), "Purple");
        purpleThread.start();
    }
}
