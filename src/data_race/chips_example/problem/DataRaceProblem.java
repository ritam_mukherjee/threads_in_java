package data_race.chips_example.problem;

import race_condition.ThreadColor;

/**
 * @apiNote problem with data race
 * 1. In this case if the increment happens in small boundary we will not experience race condition
 * 2. But if increment happen with higher limit we can experience data
 * every time we will get different output.
 */
class PartyShopping extends Thread {

    public static int bagsOfChips = 0; // start with one on the list

    public PartyShopping(String name) {
        this.setName(name);
    }

    public void run() {
        for (int i = 0; i < 10_000_000; i++) {
            bagsOfChips++;
        }
    }
}
public class DataRaceProblem {
    public static void main(String[] args) throws InterruptedException {
        Thread greenThread=new PartyShopping("green");
        Thread redThread=new PartyShopping("red");
        redThread.start();
        greenThread.start();

        redThread.join();
        greenThread.join();

        System.out.println(ThreadColor.ANSI_BLUE+"No of chips will be purchases "+PartyShopping.bagsOfChips);
    }
}
