package liveliness.abandoned_lock.solution;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class AbandonedLockScenario {
    public static void main(String[] args) {
        Lock spoonA = new ReentrantLock();
        Lock spoonB = new ReentrantLock();
        Lock spoonC = new ReentrantLock();
        /*
        here spoons are based on priority where red philosopher and rule that everyone should first aquire
                highest priority lock then lowest priority lock
                that is applicable for red and green thread but not for cyan thread.
        hence swap the priority*/
        new Philosopher("red", spoonA, spoonB).start();
        new Philosopher("green", spoonB, spoonC).start();
        new Philosopher("cyan", spoonA, spoonC).start();
    }
}
