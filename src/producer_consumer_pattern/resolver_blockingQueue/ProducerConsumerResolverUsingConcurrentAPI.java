package producer_consumer_pattern.resolver_blockingQueue;

import producer_consumer_pattern.soup_example.blocking_queue.pipeline.ThreadColor;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class ProducerConsumerResolverUsingConcurrentAPI {

    BlockingQueue<Integer> queue = new ArrayBlockingQueue<>(50);
    int count=0;
    void produce() {
        queue.add(count++);
    }


    void consume() {
        queue.poll();
    }

    public static void main(String[] args) throws InterruptedException {

        ProducerConsumerResolverUsingConcurrentAPI runner=new ProducerConsumerResolverUsingConcurrentAPI();


        Runnable produceTask = () -> {
            for (int i = 0; i < 50; i++) {
                runner.produce();
            }
            System.out.println(ThreadColor.ANSI_GREEN + " Producing Done");
        };

        Runnable consumeTask = () -> {
            for (int i = 0; i < 50; i++) {
                runner.consume();
            }
            System.out.println(ThreadColor.ANSI_RED + " Consuming Done");
        };



        Thread producer_thread = new Thread(produceTask);
        Thread consumer_thread = new Thread(consumeTask);

        consumer_thread.start();
        producer_thread.start();

        consumer_thread.join();
        producer_thread.join();

        System.out.println(runner.queue.size());

    }
}
