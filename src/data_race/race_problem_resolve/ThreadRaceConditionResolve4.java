package data_race.race_problem_resolve;

import thread_memory.ThreadColor;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * This program lock and unlock not properly protecting critical section
 * Hence giving unwanted output everytime.
 */
public class ThreadRaceConditionResolve4 {
    public static void main(String[] args) throws InterruptedException {
        AtomicInteger count = new AtomicInteger();
        Lock lock=new ReentrantLock();

        Runnable incrementingThread = () -> {


            for (int i = 0; i < 1000000; i++) {
                lock.lock();
                count.getAndIncrement();
                lock.unlock();
            }

            System.out.println(
                    ThreadColor.getThreadColor(Thread.currentThread().getName()) +
                            "The processed count variable and the no is:" + count);

        };

        Runnable decrementingThread = () -> {

            for (int i = 0; i < 1000000; i++) {
                lock.lock();
                count.getAndDecrement();
                lock.unlock();
            }
            System.out.println(
                    ThreadColor.getThreadColor(Thread.currentThread().getName()) +
                            "The processed count variable and the no is:" + count);

        };

        Thread redThreadInc = new Thread(incrementingThread, "red");
        Thread cyanThreadInc = new Thread(incrementingThread, "cyan");
        Thread greenThreadInc = new Thread(incrementingThread, "green");
        Thread yellowThreadInc = new Thread(incrementingThread, "yellow");
        Thread purpleThreadInc = new Thread(incrementingThread, "purple");

        Thread redThreadDec = new Thread(decrementingThread, "red");
        Thread cyanThreadDec = new Thread(decrementingThread, "cyan");
        Thread greenThreadDec = new Thread(decrementingThread, "green");
        Thread yellowThreadDec = new Thread(decrementingThread, "yellow");
        Thread purpleThreadDec = new Thread(decrementingThread, "purple");

        redThreadInc.start();
        cyanThreadInc.start();
        greenThreadInc.start();
        yellowThreadInc.start();
        purpleThreadInc.start();

        redThreadDec.start();
        cyanThreadDec.start();
        greenThreadDec.start();
        yellowThreadDec.start();
        purpleThreadDec.start();

        redThreadInc.join();
        cyanThreadInc.join();
        greenThreadInc.join();
        yellowThreadInc.join();
        purpleThreadInc.join();


        redThreadDec.join();
        cyanThreadDec.join();
        greenThreadDec.join();
        yellowThreadDec.join();
        purpleThreadDec.join();

    }
}
