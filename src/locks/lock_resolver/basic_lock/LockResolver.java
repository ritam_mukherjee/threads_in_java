package locks.lock_resolver.basic_lock;



import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @purope :   This program demonstrate how by log pattern we can achieve same functionality like synchronization
 * @apiNote : Also through this method we can see that a blocking call happen to other threads those not acquired lock
 */
public class LockResolver {
    public static void main(String[] args) {
        Lock lock = new ReentrantLock();

        Runnable runnable = () -> {
           /* initial phase where all threads are trying to get lock*/
            System.out.println(ThreadColor.getThreadColor.apply(Thread.currentThread().getName())
                    + Thread.currentThread().getName() +
                    " thread.... is trying to get lock");

            /*acquired lock*/
            lock.lock();
            System.out.println(ThreadColor.getThreadColor.apply(Thread.currentThread().getName())
                    + Thread.currentThread().getName() +
                    " thread.. got lock performing safe operation");

            for (int i = 1; i <= 5; i++) {
                try {
                    System.out.println(ThreadColor.getThreadColor.apply(Thread.currentThread().getName())+"\t\t"
                            + Thread.currentThread().getName()
                            + "thread is  now executing :" + i);
                    Thread.sleep(3000);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            /*releasing lock*/
            System.out.println(ThreadColor.getThreadColor.apply(Thread.currentThread().getName())
                    + Thread.currentThread().getName() +
                    " thread.. is going to release lock");
            lock.unlock();

        };
        Thread green_thread = new Thread(runnable, "green");
        Thread red_thread = new Thread(runnable, "red");
        Thread cyan_thread = new Thread(runnable, "cyan");
        Thread purple_thread = new Thread(runnable, "purple");
        Thread blue_thread = new Thread(runnable, "blue");

        green_thread.start();
        red_thread.start();
        cyan_thread.start();
        purple_thread.start();
        blue_thread.start();

    }
}
