package thread_creation;



public class ThreadCreation2 {
    public static void main(String[] args) throws InterruptedException {
        /*with single instance only once thread can be created*/
         Thread thread=new Thread(() -> {
             for (int i = 0; i < 5; i++) {
                 System.out.println(ThreadColor.ANSI_GREEN+"Thread start");
             }
         });
         thread.start();
         thread.join();

         /*thread.start();*/
    }
}
