package concurrent_collection;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * reduce()
 * search()
 */
public class ConcurrentHashMap1 {
    public static void main(String[] args) {

        ConcurrentHashMap<String, List<String>> map=new ConcurrentHashMap<>();
        
        map.put("Ritam", Arrays.asList("java","python","docker","nosql","spring"));
        map.put("Poulami", Arrays.asList("fusion","oracle","workdays","peoplesoft"));
        map.put("Astha", Arrays.asList("java","oracle","spring","hibernate"));
        map.put("Tina", Arrays.asList("java","spring"));
        map.put("Ramisha", Arrays.asList("english","softskill","hr"));


        /*find the maximum skill of an individual*/
        Integer maxSkill = map.reduce(10,
                (name, skills) -> skills.size(), Integer::max);

        System.out.println(ThreadColor.ANSI_PURPLE+"Maximum skill of an individual"+maxSkill);

       /* find the person having maximum skill*/
        String name = map.search(10,
                (name1, skills1) -> skills1.size() == maxSkill ? name1 : null);

        System.out.println(ThreadColor.ANSI_GREEN+"Person having maximum skill: "+name);

        System.out.println(ThreadColor.ANSI_CYAN+"All skills are:");
        System.out.println(map.<String>reduce(10,
                (name2, skills2) -> skills2.stream().collect(Collectors.joining(",")),
                (s1, s21) -> s1+";\n"+ s21));


        System.out.println(ThreadColor.ANSI_BLUE+"Total skills are are:"+
                map.reduce(10, (name3, skill3) -> skill3.size(), Integer::sum));

        System.out.println(ThreadColor.ANSI_YELLOW+"Person having more than five skills are:"+
                map.<String>
                reduce(10, (name4, skill4) -> skill4.size() > 3 ? name4 : null,
                        (s, s2) -> s + "," + s2));




    }
}
