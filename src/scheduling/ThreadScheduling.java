package scheduling;

/**
 * Two threads chopping vegetables
 */

class Worker extends Thread{

    public int counter = 0;
    public static boolean incrementSwitch = true;

    public Worker(String name) {
        this.setName(name);
    }

    public void run() {
        while(incrementSwitch) {
            System.out.println(
                    ThreadColor.getThreadColor.apply(Thread.currentThread().getName())
                            +this.getName() + " is incrementing count");
            counter++;
        }
    }
}

public class ThreadScheduling {
    public static void main(String args[]) throws InterruptedException {
        Worker greenThread = new Worker("green");
        Worker redThread = new Worker("red");

        greenThread.start();                    // Barron start chopping
        redThread.start();                    // Olivia start chopping
        Thread.sleep(1000);          // continue chopping for 1 second
        Worker.incrementSwitch = false; // stop chopping

        greenThread.join();
        redThread.join();
        System.out.format(ThreadColor.ANSI_BLUE+"green thread incremented %d counter.\n", greenThread.counter);
        System.out.format(ThreadColor.ANSI_BLUE+"red thread incremented %d counter.\n", redThread.counter);
    }
}