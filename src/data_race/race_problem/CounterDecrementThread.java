package data_race.race_problem;

import thread_memory.ThreadColor;

class CounterDecrementThread extends Thread {

    int count = 0;

    @Override
    public void run() {

        for (int i = 0; i < 10000000; i++) {
            count--;
        }
        System.out.println(
                ThreadColor.getThreadColor(Thread.currentThread().getName()) +
                        "The processed count variable and the no is:" + count);

    }
}
