package data_race.race_problem;


public class ThreadRaceConditionProblem1 {


    public static void main(String[] args) throws InterruptedException {

        CounterIncrementerThread incrementingThread=new CounterIncrementerThread();

        Thread redThreadInc = new Thread(incrementingThread, "red");
        Thread cyanThreadInc = new Thread(incrementingThread, "cyan");
        Thread greenThreadInc = new Thread(incrementingThread, "green");
        Thread yellowThreadInc = new Thread(incrementingThread, "yellow");
        Thread purpleThreadInc = new Thread(incrementingThread, "purple");

        redThreadInc.start();
        cyanThreadInc.start();
        greenThreadInc.start();
        yellowThreadInc.start();
        purpleThreadInc.start();

        redThreadInc.join();
        cyanThreadInc.join();
        greenThreadInc.join();
        yellowThreadInc.join();
        purpleThreadInc.join();

    }
}
