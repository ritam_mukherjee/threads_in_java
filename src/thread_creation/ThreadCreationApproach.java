package thread_creation;

import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/*Marked in Red*/
class CustomExtendThread extends Thread {

    @Override
    public void run() {
        for (int i = 1; i <= 5; i++) {
            System.out.println(ThreadColor.ANSI_RED + Thread.currentThread().getName() + " is executing, Count:" + i);

        }
    }
}


class CustomImplementRunnable implements Runnable {
    @Override
    public void run() {
        for (int i = 1; i <= 5; i++) {
            System.out.println(ThreadColor.ANSI_YELLOW + Thread.currentThread().getName() + " is executing, Count:" + i);

        }
    }
}


class CustomImplementCallable implements Callable {
    @Override
    public String call() {
        for (int i = 1; i <= 5; i++) {
            System.out.println(ThreadColor.ANSI_GREEN + Thread.currentThread().getName() + " is executing, Count:" + i);

        }
        return Thread.currentThread().getName();
    }
}

public class ThreadCreationApproach {

    public static void main(String[] args) throws InterruptedException {

        System.out.println(ThreadColor.ANSI_RESET + "------------APPROACH 1: By extending Thread Class-------------");
        Thread extendedThread = new CustomExtendThread();
        extendedThread.setName("Extended Thread");
        extendedThread.start();

        extendedThread.join();

        System.out.println(ThreadColor.ANSI_RESET + "------------APPROACH 2: By Implementing Runnable Interface--------");
        /*Passing a runnable instance in a thread class*/
        Thread runnableThread = new Thread(new CustomImplementRunnable(), "Runnable Thread");
        runnableThread.start();
        runnableThread.join();


        /*Rnnable & callable are two functiona interfaces which represents functionality not just data*/

        System.out.println(ThreadColor.ANSI_RESET + "--------APPROACH 3: by using Anonymous inner class-------");
        Runnable innerClassRunnable = new Runnable() {
            @Override
            public void run() {
                for (int i = 1; i <= 5; i++) {
                    System.out.println(ThreadColor.ANSI_CYAN + Thread.currentThread().getName() + " is executing, Count:" + i);

                }
            }
        };
        Thread innerClassThread = new Thread(innerClassRunnable, "Inner Class Thread");
        innerClassThread.start();
        innerClassThread.join();


        System.out.println(ThreadColor.ANSI_RESET + "--------APPROACH 4 by using Lambda Expression-------");
        Runnable lamdaExpressionRunnable = () -> {
            for (int i = 1; i <= 5; i++) {
                System.out.println(ThreadColor.ANSI_PURPLE + Thread.currentThread().getName() + " is executing, Count:" + i);

            }
        };
        Thread lambdaExpressionThread = new Thread(lamdaExpressionRunnable, "Lambda Expression Thread");
        lambdaExpressionThread.start();
        lambdaExpressionThread.join();



        System.out.println(ThreadColor.ANSI_RESET + "--------APPROACH 5: by using Executor Service-------");
        ExecutorService service = Executors.newFixedThreadPool(5);
        Runnable executorServiceRunnable = () -> {
            for (int i = 1; i <= 5; i++) {
                System.out.println(ThreadColor.ANSI_BLUE + Thread.currentThread().getName() + " is executing, Count:" + i);

            }
        };
        service.submit(executorServiceRunnable);
        /*putting an halt in Main thread*/
        Thread.sleep(1000);

        System.out.println(ThreadColor.ANSI_RESET + "--------APPROACH 6: by using Callable interface-------");
        CustomImplementCallable customImplementCallable = new CustomImplementCallable();
        service.submit(customImplementCallable);

        Thread.sleep(1000);
        service.shutdown();

        System.out.println(ThreadColor.ANSI_RESET + "--------APPROACH 7 by using Method Reference------");
        Thread methodReferenceThread=new Thread(ThreadCreationApproach::runThread);
        methodReferenceThread.start();
        methodReferenceThread.join();

        System.out.println(ThreadColor.ANSI_RESET + "--------APPROACH 8 by using Asynchronous Approach------");
        CompletableFuture future=CompletableFuture.supplyAsync(() -> {
            for (int i = 1; i <= 5; i++) {
                System.out.println(ThreadColor.ANSI_BLACK + Thread.currentThread().getName() + " is executing, Count:" + i);

            }
            return "done";
        });
        future.join();
    }

    private static void runThread() {
        for (int i = 1; i <= 5; i++) {
            System.out.println(ThreadColor.ANSI_WHITE + Thread.currentThread().getName() + " is executing, Count:" + i);

        }
    }
}
