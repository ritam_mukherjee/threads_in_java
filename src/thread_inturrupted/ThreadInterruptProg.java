package thread_inturrupted;

import life_cycle.ThreadColor;
import thread_creation.ThreadCreationApproach;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.IntStream;

public class ThreadInterruptProg {


    public static void main(String[] args) {

        Runnable runnable=()->{

            IntStream.range(0,1000000).forEach(value -> {
                System.out.println(ThreadColor.ANSI_GREEN+Thread.currentThread().getName()+" Is Executing, value:"+value);

                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if(Thread.currentThread().isInterrupted()){
                    System.out.println(ThreadColor.ANSI_RED+Thread.currentThread().getName()
                            +" Is interrupted while, value:"+value);
                    Thread.currentThread().interrupt();
                }

            });

        };
        Thread calledThread= new Thread(runnable);
        calledThread.start();
        calledThread.interrupt();
        calledThread.interrupt();
    }
}
