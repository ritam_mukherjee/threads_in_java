package liveliness.deadlock; /**
 * Three philosophers, thinking and eating fried chicken
 */

import java.util.concurrent.locks.*;

public class DeadlockDemo1 {
    public static void main(String[] args) {

        /*three instance of spoons need to create*/
        Lock spoonA = new ReentrantLock();
        Lock spoonB = new ReentrantLock();
        Lock spoonC = new ReentrantLock();

       /* each philosopher needs two of three spoons*/
        new Philosopher("red", spoonA, spoonB).start();
        new Philosopher("green", spoonB, spoonC).start();
        new Philosopher("cyan", spoonC, spoonA).start();
    }
}