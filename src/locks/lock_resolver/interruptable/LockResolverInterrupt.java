package locks.lock_resolver.interruptable;



import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * here by aquiring interruptable lock and if interruption happened we can't get predicted result
 */
public class LockResolverInterrupt {
    public static void main(String[] args) {
        Lock lock = new ReentrantLock();

        Runnable runnable = () -> {
           /* initial phase where all threads are trying to get lock*/
            System.out.println(ThreadColor.getThreadColor.apply(Thread.currentThread().getName())
                    + Thread.currentThread().getName() +
                    " thread.... is trying to get lock");

            /*acquired lock*/
            try {
                lock.lockInterruptibly();
            } catch (InterruptedException e) {
                System.out.println(locks.try_lock.try_lock_nonInterruptable.ThreadColor.ANSI_WHITE+" Interruption Happened...");

            }

            /*releasing lock*/
            System.out.println(ThreadColor.getThreadColor.apply(Thread.currentThread().getName())
                    + Thread.currentThread().getName() +
                    " thread.. is going to release lock");
            try {
                lock.unlock();
                System.out.println(ThreadColor.getThreadColor.apply(Thread.currentThread().getName())
                        + Thread.currentThread().getName() +
                        " thread.. got lock performing safe operation");

                for (int i = 1; i <= 5; i++) {
                    try {
                        System.out.println(ThreadColor.getThreadColor.apply(Thread.currentThread().getName())+"\t\t"
                                + Thread.currentThread().getName()
                                + "thread is  now executing :" + i);
                        Thread.sleep(3000);
                    } catch (Exception e) {
                        System.out.println(ThreadColor.ANSI_WHITE+" Interruption Happened..."
                                +Thread.currentThread().getName()+" thread");


                    }
                }
            }catch (RuntimeException re){
                System.out.println(ThreadColor.ANSI_WHITE+" Runtime exception Happened...");

            }
        };
        Thread green_thread = new Thread(runnable, "green");
        Thread red_thread = new Thread(runnable, "red");
        Thread cyan_thread = new Thread(runnable, "cyan");
        Thread purple_thread = new Thread(runnable, "purple");
        Thread blue_thread = new Thread(runnable, "blue");

        green_thread.start();
        red_thread.start();
        cyan_thread.start();
        purple_thread.start();
        blue_thread.start();

        red_thread.interrupt();
        green_thread.interrupt();
    }
}
