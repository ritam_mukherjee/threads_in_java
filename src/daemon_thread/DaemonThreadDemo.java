package daemon_thread;

/**
 * Barron finishes cooking while Olivia cleans
 */

class SpawnThread extends Thread {
    public void run() {
        while (true) {
            System.out.println("\t" +
                    ThreadColor.getThreadColor.apply(Thread.currentThread().getName()) +
                    Thread.currentThread().getName() + " is executing.");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

public class DaemonThreadDemo {
    public static void main(String[] args) throws InterruptedException {
        Thread greenThread = new SpawnThread();
        greenThread.setName("green");
        /*we can make green thread detach by setting daemon and we can see still that will run*/
        greenThread.setDaemon(true);

        /*green thread is now child of red thread which will wait until green thread is running*/
        greenThread.start();

        Thread.currentThread().setName("red");
        System.out.println(
                ThreadColor.getThreadColor.apply(Thread.currentThread().getName()) +
                        Thread.currentThread().getName() + " is executing.");
        Thread.sleep(600);
        System.out.println(
                ThreadColor.getThreadColor.apply(Thread.currentThread().getName()) +
                        Thread.currentThread().getName() + " is executing.");
        Thread.sleep(600);
        System.out.println(
                ThreadColor.getThreadColor.apply(Thread.currentThread().getName()) +
                        Thread.currentThread().getName() + " is executing.");
        Thread.sleep(600);
        System.out.println(
                ThreadColor.getThreadColor.apply(Thread.currentThread().getName()) +
                        Thread.currentThread().getName() + " is done.........");
    }
}