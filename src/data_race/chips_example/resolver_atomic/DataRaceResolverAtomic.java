package data_race.chips_example.resolver_atomic;



import java.util.concurrent.atomic.AtomicInteger;

/**
 * Earlier problem we have noticed that if increment happen in higher limit we have seen data race.
 * But if we use mutex successfully we will not experience any data race.
 */
class PartyShopping extends Thread {

    public static AtomicInteger  bagsOfChips= new AtomicInteger(0); // start with one on the list


    public PartyShopping(String name) {
        this.setName(name);
    }

    public void run() {
        for (int i = 0; i < 10_000_000; i++) {
            bagsOfChips.incrementAndGet();
        }
    }
}

public class DataRaceResolverAtomic {
    public static void main(String[] args) throws InterruptedException {

        long beforeExecutionTime = System.currentTimeMillis();
        Thread greenThread=new PartyShopping("green");
        Thread redThread=new PartyShopping("red");
        redThread.start();
        greenThread.start();

        redThread.join();
        greenThread.join();

        long afterExecutionTime = System.currentTimeMillis();
        System.out.println(ThreadColor.ANSI_BLUE+"No of chips will be purchases "+PartyShopping.bagsOfChips);
        System.out.println(ThreadColor.ANSI_CYAN+"Time taken :"+(afterExecutionTime-beforeExecutionTime));
    }
}
