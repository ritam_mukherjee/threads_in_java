package blocking_queue;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class BlockingQueueProgram {
    public static void main(String[] args) {
        BlockingQueue<Integer> blockingQueue=new ArrayBlockingQueue<>(10);

        Thread producer=new Thread(() -> {
            int counter=0;
            while (true){
                try{
                    System.out.println(ThreadColor.ANSI_GREEN+"Putting number in queue:"+counter);
                    blockingQueue.put(counter);
                    counter++;
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        Thread consumer=new Thread(() -> {
            while (true){
                try{
                    int number=blockingQueue.take();
                    System.out.println(ThreadColor.ANSI_RED+"getting  number from queue:"+number);
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        producer.start();
        consumer.start();
    }
}
