package liveliness.abandoned_lock.problem;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class AbandonedLockScenario {
    public static void main(String[] args) {
        Lock spoonA = new ReentrantLock();
        Lock spoonB = new ReentrantLock();
        Lock spoonC = new ReentrantLock();

        /*here locks are supplied priority wise so that deadlock not happen due to race condition*/
        new Philosopher("red", spoonA, spoonB).start();
        new Philosopher("green", spoonB, spoonC).start();
        new Philosopher("cyan", spoonA, spoonC).start();
    }
}
