package data_race.chips_example.resolver_challenge;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Earlier problem we have noticed that if increment happen in higher limit we have seen data race.
 * But if we use mutex successfully we will not experience any data race.
 */
class PartyShopping extends Thread {

    public static int bagsOfChips = 0; // start with one on the list
    /*pencil object act as mutex*/
    private static Lock pencil = new ReentrantLock();

    public PartyShopping(String name) {
        this.setName(name);
    }

    public void run() {
        pencil.lock();
        for (int i = 0; i < 20; i++) {
            bagsOfChips++;
            try {
                Thread.sleep(500);
                System.out.println(
                        ThreadColor.getThreadColor(Thread.currentThread().getName())
                +Thread.currentThread().getName()+" thread is now waiting");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        pencil.unlock();
    }
}

public class DataRaceResolver {
    public static void main(String[] args) throws InterruptedException {

        long beforeExecutionTime = System.currentTimeMillis();
        Thread greenThread=new PartyShopping("green");
        Thread redThread=new PartyShopping("red");
        redThread.start();
        greenThread.start();

        redThread.join();
        greenThread.join();

        long afterExecutionTime = System.currentTimeMillis();
        System.out.println(ThreadColor.ANSI_BLUE+"No of chips will be purchases "+PartyShopping.bagsOfChips);
        System.out.println(ThreadColor.ANSI_CYAN+"Time taken :"+(afterExecutionTime-beforeExecutionTime));
    }
}
