package producer_consumer_pattern.soup_example.blocking_queue.without_piepline.queue_full;

/**
 * Producers serving soup for Consumers to eat
 */

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * 1. Here we are simulating a situation where producing happen much faster than consumer.
 * 2. we will see one it's reaches the limit it will throw exception.
 */
class SoupProducer extends Thread {

    private BlockingQueue servingLine;
    private String name;
    public SoupProducer(BlockingQueue servingLine,String name) {
        this.servingLine = servingLine;
        this.name=name;
        Thread.currentThread().setName(name);
    }

    public void run() {
        for (int i=0; i<20; i++) { // serve 20 bowls of soup
            try {
                servingLine.add("Bowl #" + i);
                System.out.format(ThreadColor.getThreadColor.apply(name)+
                        "Producer Served Bowl #%d - remaining capacity: %d\n", i, servingLine.remainingCapacity());
                Thread.sleep(200); // time to serve a bowl of soup
            } catch (Exception e) {
                System.out.println(ThreadColor.ANSI_BLACK+e.getMessage());
            }
        }
    }
}

class SoupConsumer extends Thread {

    private BlockingQueue servingLine;
    private String name;

    public SoupConsumer(BlockingQueue servingLine,String name) {
        this.servingLine = servingLine;
        this.name=name;
        Thread.currentThread().setName(name);
    }

    public void run() {
        while (true) {
            try {
                String bowl = (String)servingLine.take();
                System.out.format(ThreadColor.getThreadColor.apply(name)+
                        "Consumer:Ate %s\n", bowl);
                Thread.sleep(300); // time to eat a bowl of soup
             } catch (Exception e) { e.printStackTrace(); }
        }
    }
}

public class ProducerConsumerDemo2 {
    public static void main(String args[]) {
        BlockingQueue servingLine = new ArrayBlockingQueue<String>(5);
        new SoupConsumer(servingLine,"red").start();
        new SoupProducer(servingLine,"green").start();
    }
}