package liveliness.abandoned_lock.problem;



import java.util.concurrent.locks.Lock;

class Philosopher extends Thread {

    private Lock leftSpoon, rightSpoon;
    /*if the count of friedChicken is less no issue but if the count is more then deadlock happens*/
    private static int friedChicken =500;

    public Philosopher(String name, Lock leftSpoon, Lock rightSpoon) {
        this.setName(name);
        this.leftSpoon = leftSpoon;
        this.rightSpoon = rightSpoon;
    }

    public void run() {
        // eat fied chicken until it's all gone
        while(friedChicken > 0) {

            // pick up spoon
            leftSpoon.lock();
            rightSpoon.lock();

            // take a piece of fried chicken
            if (friedChicken > 0) {
                friedChicken--;
                System.out.println(
                        ThreadColor.getThreadColor.apply(this.getName())
                                + "The "+this.getName() + " took a piece! fried chicken remaining: "
                                + friedChicken);



            }
            if(friedChicken==100)
                throw new RuntimeException("reached threshold count");
            // put down chopsticks
            rightSpoon.unlock();
            leftSpoon.unlock();
        }
    }
}
