package priority_blocking_queue;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;

/**
 * NOT COMPLETED
 */
public class PriorityBlockingQueueProgram {
    public static void main(String[] args) {
        BlockingQueue<Integer> blockingQueue=new PriorityBlockingQueue<>(10);

        Thread producer=new Thread(() -> {
            int counter=0;
            while (true){
                try{
                    blockingQueue.put(5);
                    blockingQueue.put(1);
                    Thread.sleep(1000);
                    blockingQueue.put(3);
                    Thread.sleep(1000);
                    blockingQueue.put(9);
                    blockingQueue.put(2);
                    Thread.sleep(1000);
                    blockingQueue.put(8);
                    blockingQueue.put(4);
                    Thread.sleep(10000);
                    blockingQueue.put(7);
                    Thread.sleep(1000);
                    blockingQueue.put(6);
                    System.out.println(ThreadColor.ANSI_GREEN+"Putting number in queue:"+counter);
                    blockingQueue.put(counter);
                    counter++;
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        Thread consumer=new Thread(() -> {
            while (true){
                try{
                    int number=blockingQueue.take();
                    System.out.println(ThreadColor.ANSI_RED+"getting  number from queue:"+number);
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        producer.start();
        consumer.start();
    }
}
