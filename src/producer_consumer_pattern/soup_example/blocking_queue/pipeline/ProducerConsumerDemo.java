package producer_consumer_pattern.soup_example.blocking_queue.pipeline; /**
 * Producers serving soup for Consumers to eat
 */

import java.util.concurrent.*;

class SoupProducer extends Thread {

    private BlockingQueue servingLine;
    String name;

    public SoupProducer(BlockingQueue servingLine,String name) {

        this.servingLine = servingLine;
        this.name=name;
    }

    public void run() {
        for (int i=0; i<20; i++) { // serve 20 bowls of soup
            try {
                servingLine.add("Bowl #" + i);
                System.out.format(ThreadColor.getThreadColor.apply(name)
                        +"Producer:Served Bowl #%d - remaining capacity: %d\n", i, servingLine.remainingCapacity());
                Thread.sleep(200); // time to serve a bowl of soup
            } catch (Exception e) { e.printStackTrace(); }
        }
        servingLine.add("no soup for you!");
        servingLine.add("no soup for you!");
    }
}

class SoupConsumer extends Thread {

    private BlockingQueue servingLine;
    private String name;

    public SoupConsumer(BlockingQueue servingLine,String name) {

        this.servingLine = servingLine;
        this.name=name;
    }

    public void run() {
        while (true) {
            try {
                String bowl = (String)servingLine.take();
                if (bowl == "no soup for you!")
                    break;
                System.out.format(ThreadColor.getThreadColor.apply(name)+
                        "Consumer:Ate %s\n", bowl);
                Thread.sleep(300); // time to eat a bowl of soup
             } catch (Exception e) { e.printStackTrace(); }
        }
    }
}

public class ProducerConsumerDemo {
    public static void main(String args[]) {
        BlockingQueue servingLine = new ArrayBlockingQueue<String>(5);

       /* added two consumer*/
        new SoupConsumer(servingLine,"cyan").start();
        new SoupConsumer(servingLine,"purple").start();
        new SoupProducer(servingLine,"red").start();
    }
}