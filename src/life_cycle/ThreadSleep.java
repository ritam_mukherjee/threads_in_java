package life_cycle;

public class ThreadSleep {
    public static void main(String[] args) {

        Thread redThread= new Thread(()->{
            for (int i = 0; i < 5; i++) {
                System.out.println(ThreadColor.ANSI_RED + Thread.currentThread().getName() +
                        "+ \" is executing, Count:\" + i");
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        },"red thread");
        redThread.start();


        Thread greenThread= new Thread(()->{
            for (int i = 0; i < 5; i++) {
                System.out.println(ThreadColor.ANSI_GREEN + Thread.currentThread().getName() +
                        "+ \" is executing, Count:\" + i");
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        },"red thread");
        greenThread.start();

    }


}
