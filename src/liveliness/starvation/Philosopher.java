package liveliness.starvation;



import java.util.concurrent.locks.Lock;

class Philosopher extends Thread {

    private Lock leftSpoon, rightSpoon;
    /*if the count of friedChicken is less no issue but if the count is more then deadlock happens*/
    private static int friedChicken =500_00;

    public Philosopher(String name, Lock leftSpoon, Lock rightSpoon) {
        this.setName(name);
        this.leftSpoon = leftSpoon;
        this.rightSpoon = rightSpoon;
    }

    public void run() {
       /* this variable keep count of how many chicken is eaten by each philosopher*/
        int chickenEaten=0;

        // eat fried chicken until it's all gone
        while(friedChicken > 0) {

            // pick up spoon
            leftSpoon.lock();
            rightSpoon.lock();

            try{
                // take a piece of fried chicken
                if (friedChicken > 0) {
                    friedChicken--;
                    chickenEaten++;
                    System.out.println(
                            ThreadColor.getThreadColor.apply(this.getName())
                                    + "The "+this.getName() + " took a piece! fried chicken remaining: "
                                    + friedChicken);

                }

            }catch(Exception e){
                System.out.println(ThreadColor.ANSI_BLACK+ "exception occurred!!!!");
            }finally {
                // put down chopsticks
                rightSpoon.unlock();
                leftSpoon.unlock();
            }


        }
        System.out.println(ThreadColor.ANSI_BLUE+"########"+" Chicken eaten by "+"the "
                +this.getName() + "thread is:"+chickenEaten);

    }
}
