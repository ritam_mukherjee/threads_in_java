package liveliness.livelock.resolver;

import java.util.Random;
import java.util.concurrent.locks.Lock;

class Philosopher extends Thread {

    private Lock leftSpoon, rightSpoon;
    /*if the count of friedChicken is less no issue but if the count is more then deadlock happens*/
    private static int friedChicken =500_000;
    private Random rps=new Random();

    public Philosopher(String name, Lock leftSpoon, Lock rightSpoon) {
        this.setName(name);
        this.leftSpoon = leftSpoon;
        this.rightSpoon = rightSpoon;
    }

    public void run() {
        while(friedChicken > 0) { // eat fried chicken until it's all gone

            // pick up spoon
            leftSpoon.lock();
           /* if the philosopher pick the leftspoon but not rightspoon it will release hte lock*/
            if(!rightSpoon.tryLock()){
                System.out.println(
                        ThreadColor.getThreadColor.apply(this.getName())
                                + "The " + this.getName() + " released the first spoons");

               leftSpoon.unlock();
                /* after release spoon for sometime philosopher will wait some time for picking the spoon again*/
                try {
                    Thread.sleep(rps.nextInt(3));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }else{
                // take a piece of fried chicken
                try {
                    if (friedChicken > 0) {
                        friedChicken--;
                        System.out.println(
                                ThreadColor.getThreadColor.apply(this.getName())
                                        + "The " + this.getName() + " took a piece! fried chicken remaining: "
                                        + friedChicken);
                    }
                }catch(Exception e){
                    System.out.println(liveliness.starvation.ThreadColor.ANSI_BLACK+ "exception occurred!!!!");
                }finally {
                    // put down spoons
                    rightSpoon.unlock();
                    leftSpoon.unlock();
                }
            }

           /* Only when philosopher get two spoons together then only livelock will not happen*/
        }
    }
}
