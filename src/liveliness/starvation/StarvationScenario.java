package liveliness.starvation;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class StarvationScenario {
    public static void main(String[] args) {
        Lock spoonA = new ReentrantLock();
        Lock spoonB = new ReentrantLock();

        /*
        here spoons are based on priority where red philosopher and rule that everyone should first aquire
                highest priority lock then lowest priority lock
                that is applicable for red and green thread but not for cyan thread.
        hence swap the priority*/
        new Philosopher("red", spoonA, spoonB).start();
        new Philosopher("green", spoonA, spoonB).start();
        new Philosopher("cyan", spoonA, spoonB).start();
        new Philosopher("yellow", spoonA, spoonB).start();
        new Philosopher("purple", spoonA, spoonB).start();
    }
}
