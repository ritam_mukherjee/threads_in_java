package liveliness.livelock.resolver; /**
 * Three philosophers, thinking and eating sushi
 */

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class LivelockResolver {
    public static void main(String[] args) {
        Lock spoonA = new ReentrantLock();
        Lock spoonB = new ReentrantLock();
        Lock spoonC = new ReentrantLock();
        new Philosopher("red", spoonA, spoonB).start();
        new Philosopher("green", spoonB, spoonC).start();
        new Philosopher("cyan", spoonC, spoonA).start();
    }
}