package synchronizations;

public class SynchronizedMethodProgram {

    public  synchronized void execute(String color) {
        for (int i = 0; i < 5; i++) {
            try {
                Thread.sleep(2000);
                System.out.println(ThreadColor.getThreadColor(color)
                        +Thread.currentThread().getName()+" thread is Now executing, count: "+i);
            } catch (InterruptedException ie) {
                System.out.println(ie);
            }
        }
    }

    public static void main(String[] args) {
        SynchronizedMethodProgram program=new SynchronizedMethodProgram();

        Thread redThread=new Thread(() -> program.execute("red"),"Red");
        redThread.start();

        Thread greenThread=new Thread(() -> program.execute("green"),"Green");
        greenThread.start();

        Thread cyanThread=new Thread(() -> program.execute("cyan"),"Cyan");
        cyanThread.start();

        Thread yellowThread=new Thread(() -> program.execute("yellow"),"Yellow");
        yellowThread.start();

        Thread purpleThread=new Thread(() -> program.execute("purple"),"Purple");
        purpleThread.start();
    }
}
