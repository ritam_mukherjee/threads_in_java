package data_race.race_problem_resolve;

import thread_memory.ThreadColor;

import java.util.concurrent.atomic.AtomicInteger;

public class ThreadRaceConditionProblem2 {
    public static void main(String[] args) throws InterruptedException {
        AtomicInteger count = new AtomicInteger();
        Runnable incrementingThread = () -> {

            for (int i = 0; i < 1000000; i++) {
                count.getAndIncrement();
            }
            System.out.println(
                    ThreadColor.getThreadColor(Thread.currentThread().getName()) +
                            "The processed count variable and the no is:" + count);

        };

        Runnable decrementingThread = () -> {

            for (int i = 0; i < 1000000; i++) {
                count.getAndDecrement();
            }
            System.out.println(
                    ThreadColor.getThreadColor(Thread.currentThread().getName()) +
                            "The processed count variable and the no is:" + count);

        };

        Thread redThreadInc = new Thread(incrementingThread, "red");
        Thread cyanThreadInc = new Thread(incrementingThread, "cyan");
        Thread greenThreadInc = new Thread(incrementingThread, "green");
        Thread yellowThreadInc = new Thread(incrementingThread, "yellow");
        Thread purpleThreadInc = new Thread(incrementingThread, "purple");

        Thread redThreadDec = new Thread(decrementingThread, "red");
        Thread cyanThreadDec = new Thread(decrementingThread, "cyan");
        Thread greenThreadDec = new Thread(decrementingThread, "green");
        Thread yellowThreadDec = new Thread(decrementingThread, "yellow");
        Thread purpleThreadDec = new Thread(decrementingThread, "purple");

        redThreadInc.start();
        redThreadInc.join();

        cyanThreadInc.start();
        cyanThreadInc.join();

        greenThreadInc.start();
        greenThreadInc.join();

        yellowThreadInc.start();
        yellowThreadInc.join();

        purpleThreadInc.start();
        purpleThreadInc.join();


        redThreadDec.start();
        redThreadDec.join();

        cyanThreadDec.start();
        cyanThreadDec.join();

        greenThreadDec.start();
        greenThreadDec.join();

        yellowThreadDec.start();
        yellowThreadDec.join();

        purpleThreadDec.start();
        //purpleThreadDec.join();

    }
}
