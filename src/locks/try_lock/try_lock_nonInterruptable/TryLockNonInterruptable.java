package locks.try_lock.try_lock_nonInterruptable;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Acquiring lock UnInterrutably
 */
public class TryLockNonInterruptable {
    public static void main(String[] args) {
        Lock lock = new ReentrantLock();

        Runnable runnable = () -> {
            if (lock.tryLock()) {
                System.out.println(ThreadColor.getThreadColor.apply(Thread.currentThread().getName())
                        + Thread.currentThread().getName() +
                        ".. got lock performing safe operation");

                /*acquiring non interrupt lock*/
                lock.lock();

                for (int i = 1; i < 5; i++) {
                    try {
                        System.out.println(ThreadColor.getThreadColor.apply(Thread.currentThread().getName())+"\t\t"
                                + Thread.currentThread().getName()
                                + " now executing :" + i);
                        Thread.sleep(3000);
                    } catch (Exception e) {
                        System.out.println(ThreadColor.ANSI_WHITE+" Interruption Happened...");
                        e.printStackTrace();
                    }
                }

                /*released lock*/
                System.out.println(ThreadColor.getThreadColor.apply(Thread.currentThread().getName())
                        + Thread.currentThread().getName() +
                        " thread.. is going to release lock");
                lock.unlock();
            } else {
                System.out.println(ThreadColor.getThreadColor.apply(Thread.currentThread().getName())
                        + Thread.currentThread().getName()
                        + ".. unable to get lock");
            }
        };
        Thread green_thread = new Thread(runnable, "green");
        Thread red_thread = new Thread(runnable, "red");
        Thread cyan_thread = new Thread(runnable, "cyan");
        Thread purple_thread = new Thread(runnable, "purple");
        Thread blue_thread = new Thread(runnable, "blue");

        green_thread.start();
        red_thread.start();
        cyan_thread.start();
        purple_thread.start();
        blue_thread.start();
    }
}
